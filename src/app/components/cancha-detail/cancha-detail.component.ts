import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import jsonData from '../../data/canchas-disponibles.json'

@Component({
  selector: 'app-cancha-detail',
  templateUrl: './cancha-detail.component.html',
  styleUrls: ['./cancha-detail.component.scss']
})
export class CanchaDetailComponent implements OnInit {
  
  id!: string|null;
  sector!: string|null;
  canchasDisponible!: any;

  constructor(private route: ActivatedRoute) {}
  
  ngOnInit() {
     this.id = this.route.snapshot.paramMap.get('id');
     this.sector = this.route.snapshot.paramMap.get('sector');
     this.loadData();
  }

  private loadData(){
    const results:any = jsonData.find(x=>x.sector===this.sector);
    this.canchasDisponible = results?.canchas.find((x:any)=>x.id===Number(this.id))
  }
}
