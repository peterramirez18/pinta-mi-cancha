import { Component, OnInit } from '@angular/core';
import canchaDisponible from '../../data/canchas-disponibles.json';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss']
})
export class CatalogoComponent implements OnInit {

  active:boolean = false;

  selected: any =  canchaDisponible[0];

  print(item: any) {
    this.selected = item;
    this.active = true;
  }

  constructor() { }

  canchasdisponibles:any [] = canchaDisponible;

  ngOnInit(): void {
  }
  // getPortada(x:any){
  //   return x.images[0]
  // }

}


