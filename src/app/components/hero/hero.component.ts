import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  ngOnInit(): void {

  }
  heros = [
    {
      id: 1,
      title: '<b>Banreservas</b> y <b>ADN</b> entregan 5 canchas totalmente remozadas en el <b>Distrito Nacional.<b/>',
      subtitle: 'Estas acciones forman parte del programa Pinta mi Cancha.',
      image: 'assets/imgs/hero-img-1.png',
      link: 'https://www.instagram.com/pintamicanchard/?hl=es-la',
      active: 'active'
    },
    {
      id: 2,
      title: 'Entrega de las <b>nueves canchas</b> remozadas por <b>REXONA</b> en colaboración con la <b>ALCALDIA</b>',
      image: '../../../assets/imgs/hero-img-2.png',
      link: 'https://www.instagram.com/p/CT8AdswPOZf/',
      active: ''
    },
    {
      id: 3,
      title: 'GATORADE y ADN entregan 7 canchas remozadas en el Distrito Nacional',
      image: '../../../assets/imgs/hero-img-3.png',
      link: 'https://www.instagram.com/p/CKoX70zBi9W/',
      active: ''
    }
  ]
  heros2 = [
    {
      slider1: [
        {
          title: '<b>Banreservas</b> y <b>ADN</b> entregan 5 canchas totalmente remozadas en el <b>Distrito Nacional.<b/>',
          subtitle: 'Estas acciones forman parte del programa Pinta mi Cancha.',
          image: 'assets/imgs/hero-img-1.png',
          link: 'https://www.instagram.com/pintamicanchard/?hl=es-la',
          active: 'active'
        }
      ],
      slider2: [
        {
          title: 'Entrega de las nueves canchas remozadas por REXONa en colaboración con la ALCALDIA',
          image: '../../../assets/imgs/hero-img-2.png',
          link: 'www.youtube.com'
        }
      ],
      slider3: [
        {
          title: 'GATORADE y ADN entregan 7 canchas remozadas en el Distrito Nacional',
          image: '../../../assets/imgs/hero-img-3.png',
          link: 'www.google.com'
        }
      ]
    }
  ]




}
