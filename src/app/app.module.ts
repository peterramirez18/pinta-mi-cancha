import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { PostulaTuCanchaComponent } from './components/postula-tu-cancha/postula-tu-cancha.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { TestimoniosComponent } from './components/testimonios/testimonios.component';
import { HeroComponent } from './components/hero/hero.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { CanchaDetailComponent } from './components/cancha-detail/cancha-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CatalogoComponent,
    PostulaTuCanchaComponent,
    FooterComponent,
    HomeComponent,
    TestimoniosComponent,
    HeroComponent,
    ContactoComponent,
    CanchaDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
