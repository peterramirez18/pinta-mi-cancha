import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostulaTuCanchaComponent } from './components/postula-tu-cancha/postula-tu-cancha.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { HomeComponent } from './components/home/home.component';
import { TestimoniosComponent } from './components/testimonios/testimonios.component';
import { CanchaDetailComponent } from './components/cancha-detail/cancha-detail.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'postula-tu-cancha', component: PostulaTuCanchaComponent},
  {path: 'catalogo', component: CatalogoComponent},
  {path: 'cancha-detail/:sector/:id', component: CanchaDetailComponent},
  {path: 'testimonios', component: TestimoniosComponent},
  {path: 'objetivo', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
